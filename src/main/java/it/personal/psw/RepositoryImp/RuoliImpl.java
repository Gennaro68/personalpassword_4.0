package it.personal.psw.RepositoryImp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import it.personal.psw.Entity.Ruoli;
import it.personal.psw.Repository.RuoliRepository;

@Repository
public class RuoliImpl {
	private RuoliRepository ruoliRepository;
	
	@Autowired
	public RuoliImpl(RuoliRepository ruoliRepository) {
		this.ruoliRepository = ruoliRepository;
	}
	
	
	public void inSerisciRuolo(Ruoli ruoli) {
		ruoliRepository.save(ruoli);
	}
	
	
	public Optional<Ruoli> findById(String id){
		return  ruoliRepository.findById(Long.parseLong(id));
	}
	
	public void save(Ruoli ruoli) {
		ruoliRepository.save(ruoli);
	}
	
	public void delete(Long idutente) {
		ruoliRepository.deleteByIdUtente(idutente);
	}
	
}
