package it.personal.psw.RepositoryImp;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import it.ObjUtil.VistaAbbonati;
import it.personal.psw.Entity.Abbonato;
import it.personal.psw.Entity.Utenti;
import it.personal.psw.Repository.AbbonatoRepository;
import lombok.extern.java.Log;

@Repository
@Log
public class AbbonatiImpl {
	public AbbonatoRepository abbonatoRepository;

	@Autowired
	public AbbonatiImpl(AbbonatoRepository abbonatoRepository) {
		this.abbonatoRepository = abbonatoRepository;
	}


	
	public List<VistaAbbonati> getAbbonati1(){
		
		//Crea l' oggetto vista e lista Abbbonati
		VistaAbbonati vista = new VistaAbbonati();
		
		List<VistaAbbonati> vista_abbonati = new ArrayList<>();
		
		List<Object[]> vettoreLista = abbonatoRepository.getAbbonati1();
		//idutente,cognome,nome,email,ruolo
		
		for (Object[] elemento : vettoreLista) {
		    vista.setId_utente(elemento[0].toString());
		    vista.setCognome(elemento[1].toString());
		    vista.setNome(elemento[2].toString());
			vista.setEmail(elemento[3].toString());
		    vista.setRuolo(elemento[4].toString());
		    vista_abbonati.add(vista);
	        vista = new VistaAbbonati();
		}
		 return	vista_abbonati;	
	}
	
	
	public Long save(String nome, String cognome,String email) {
		//Inserire i parametri di controllo della mail
		Abbonato abb = new Abbonato();
		abb.setCognome(cognome);
		abb.setNome(nome);
		abb.setEmail(email);
		abbonatoRepository.save(abb);
		return abb.getId_abbonato();
	}
	
	public void save(Abbonato abbonato) {
		abbonatoRepository.save(abbonato);
	}
	
	public String getEmail(String email) {
		return abbonatoRepository.getByEmail(email);	
	}
	
	public Long getUtentiIdFromUtentiId(String email) {
		return abbonatoRepository.getByUtenti_id(email);
	}
	
	public void deleteAbbonato(Long utenti_id) {
		abbonatoRepository.deledeleteByUtenti_id(utenti_id);
		
	}
	
	
	
		
}
