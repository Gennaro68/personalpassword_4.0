package it.personal.psw.RepositoryImp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import it.personal.psw.Entity.ArchivioUtenze;
import it.personal.psw.Repository.ArchivioRepository;
import lombok.extern.java.Log;

@Repository
@Log
public class ArchivioImpl {
	private ArchivioRepository archivio;
	
	@Autowired
	public ArchivioImpl(ArchivioRepository archivio) {
		this.archivio = archivio;
	}
	
	public void addArchivioUtenze(ArchivioUtenze arch) {
		archivio.save(arch);
		
	}	
	
	public Optional<ArchivioUtenze> findById(String id){
		Long myId = Long.parseLong(id);
		return archivio.findById(myId);
	}
	
	public List<ArchivioUtenze> findByUsername(String username){
		
		return archivio.findByUsername(username);
	}
	 
	public void deleteRigaArchivioUtenze(Integer id) {
		log.info("deleteRigaArchivioUtenze-->"+id.toString());
		Long myId = Long.parseLong(id.toString());
		archivio.deleteById(myId);
	}
	
	
	
}
