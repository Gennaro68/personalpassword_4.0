package it.personal.psw.RepositoryImp;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.FluentQuery.FetchableFluentQuery;
import org.springframework.stereotype.Repository;

import it.personal.psw.Entity.Utenti;
import it.personal.psw.Repository.UtentiRepository;
import lombok.extern.java.Log;

@Repository
@Log
public class UtentiImpl  {
	private UtentiRepository utentiRepository;
	
	@Autowired
	public UtentiImpl (UtentiRepository utentiRepository) {
		this.utentiRepository=utentiRepository;
	}
	
	public Long inserisciUtenti(Utenti utenti) {
		utentiRepository.save(utenti);
		return utenti.getId();
	}
	
	public boolean verificaUtenti(String username) {
		boolean ritorno = false;
		log.info("===verificaUtenti===");
		
		Utenti utenti = utentiRepository.findByUserid(username);
		
		if(utenti.getAttivo().equals("Si")) {
			ritorno=true;
		}
		
		return ritorno;
	}
	
	
	public Utenti getUtenti(String userid) {
		return utentiRepository.findByUserid(userid);
	}
	
	public Optional<Utenti> getUtentiById(Long id) {
		return utentiRepository.findById(id);
	}
	
	public void updateUtenti(Utenti utenti) {
		utentiRepository.save(utenti);
	}
	
	public Long getIdFromUserid(String userid) {
		return	utentiRepository.findByUserid1(userid);
	}
	
    public void deleteByuserid(Long userid) {
    	utentiRepository.deleteById(userid);
    }
	
}
