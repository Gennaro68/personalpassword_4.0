package it.personal.psw;

import java.util.Properties;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import it.personal.psw.Entity.Ruoli;
import it.personal.psw.Entity.Utenti;
import it.personal.psw.Repository.RuoliRepository;
import it.personal.psw.Repository.UtentiRepository;
import lombok.extern.java.Log;
import it.ObjUtil.*;

@Log
@SpringBootApplication
public class PersonalPasswordApplication {
		
	
	public static void main(String[] args) {
		SpringApplication.run(PersonalPasswordApplication.class, args);
	}
	
//	@Bean
//	<S> CommandLineRunner run(RuoliRepository rr,UtentiRepository ur) {
//		return args->{
//						
//			Utenti utente = new Utenti();
//			
//			
//			utente.setId(0L);
//			utente.setAttivo("Si");
//			utente.setPassword(new BCryptPasswordEncoder().encode("password"));
//			utente.setRuoli(null);
//			utente.setUserid("username");
//			
//			rr.save(utente);
//			
//			Ruoli ruoli = new Ruoli();
//			ruoli.setId(1L);
//			ruoli.setRuolo("USER");
//			ruoli.setUtente(utente);
//			
//			ur.save(ruoli);
//			
//			
//			
//		};
//	}   		

	

	

}
