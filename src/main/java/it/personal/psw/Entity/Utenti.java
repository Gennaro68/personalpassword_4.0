package it.personal.psw.Entity;

import static jakarta.persistence.GenerationType.AUTO;

import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.*;

import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Utenti {

	@Id
	@GeneratedValue(strategy = AUTO)
	private Long id;

	private String userid;
	private String password;
	private String attivo = "Si";

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "utente")
	private Set<Ruoli> ruoli = new HashSet<>();

}
