package it.personal.psw.Security;

import java.io.IOException;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;

@Log
public class CustomAuthenticationFailureHandler implements AuthenticationFailureHandler {

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {
		
		String msg="Attenzione! Errore di autenticazione!";
		
		
		log.info("CustomAuthenticationFailureHandler-->"+"/loginError"+msg);
		
		//response.sendRedirect("/loginError");
		response.sendRedirect("/login?messaggio="+msg);
		//response.sendRedirect("/errore");
	}

}
