package it.personal.psw.Security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import it.personal.psw.Entity.Utenti;
import it.personal.psw.Repository.RuoliRepository;
import it.personal.psw.Repository.UtentiRepository;
import lombok.SneakyThrows;
import lombok.extern.java.Log;

@Log
@Service
public class CustomUserDetailsService implements UserDetailsService {
	
	private UtentiRepository utentiRepository;
	//private RuoliRepository ruoliRepository;
	
	@Autowired
	public CustomUserDetailsService(@Qualifier("utentiRepository") UtentiRepository utentiRepository) {
		this.utentiRepository=utentiRepository;
	}
	
//	@Autowired
//	public CustomUserDetailsService(@Qualifier("ruoliRepository") RuoliRepository ruoliRepository) {
//		this.ruoliRepository=ruoliRepository;
//	}
	
	

	@Override
	@SneakyThrows
	public UserDetails loadUserByUsername(String username){
		String errMsg="";
		
		if(username==null || username.length()<5) {
			errMsg="Nome utente non valido o troppo corto";
			log.warning(errMsg);
			throw new UsernameNotFoundException(errMsg);
		}
		
		Utenti utente = this.getUtenti(username);
		
		if(utente==null) {
			errMsg=String.format("Utente %s non trovato", username);
			log.warning(errMsg);
			
			throw new UsernameNotFoundException(errMsg);
		}
		
		//Adesso dobbiamo costruire la user e lo facciamo con lo UserBuilder
		UserBuilder builder=null;
		builder = org.springframework.security.core.userdetails.User.withUsername(utente.getUserid().trim());
		builder.disabled((utente.getAttivo().equals("Si") ? false:true));
		builder.password(utente.getPassword().trim());
		
		String[] profili = utente.getRuoli()
				.stream().map(a->"ROLE_"+a.getRuolo().trim()).toArray(String[]::new);
		
		//Vai a caricare le autorities
		
		builder.authorities(profili);
		
		return builder.build();
	}

	private Utenti getUtenti(String username) {
		Utenti utente = null;
		
		try {
			utente = utentiRepository.findByUserid(username);
		}catch(Exception e){
			String errMsg = String.format("Errore nel corso della ricerca dell'utente");
			log.warning(errMsg);
		}	
			
		return utente;
		
	}
	
}
