package it.personal.psw.Security;

import java.io.IOException;
import java.net.http.HttpHeaders;

import org.springframework.http.ResponseCookie;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.http.HttpHeaders.*;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;

@Log
public class CustomSuccessHandler implements AuthenticationSuccessHandler{

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,Authentication authentication) throws IOException, ServletException {

//************************************************************************************		
//		httpOnly
//		può essere utilizzato solo dal server web per gestire lo stato 
//		dell'utente e non può essere letto o modificato da JavaScript eseguito sul lato client
//************************************************************************************		
		//response.sendRedirect("/index/"+ authentication.getName());    
		//String pagina_admin="/index/"+ authentication.getName();
		
		log.info("Viene creato un cookie");
		ResponseCookie springCookie = ResponseCookie.from("user-id",authentication.getName())
				.httpOnly(true)
				.secure(false)
				.path("/")
				.maxAge(600)
				.build();
		
		log.info("cookie ->"+springCookie.toString());
		log.info("cookie--authentication.getName()--->"+authentication.getName());
		
		//org.springframework.http.HttpHeaders.SET_COOKIE
		
		response.addHeader(org.springframework.http.HttpHeaders.SET_COOKIE, springCookie.toString());
		
		
		
		log.info("CustomSuccessHandler--->" + "/adminPage");
		
		
		response.sendRedirect("/adminPage");
		
	}

}
