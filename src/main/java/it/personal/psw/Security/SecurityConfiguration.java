package it.personal.psw.Security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import lombok.SneakyThrows;
import lombok.extern.java.Log;

@Configuration
@EnableWebSecurity
@Log
public class SecurityConfiguration  {
	private UserDetailsService userDetailsService;
	
	@Autowired
	public SecurityConfiguration(UserDetailsService userDetailsService) {
		this.userDetailsService=userDetailsService;
	}
	
	@Autowired
	@SneakyThrows
	public void configureGlobal(AuthenticationManagerBuilder auth) {
		auth.userDetailsService(userDetailsService)
		.passwordEncoder(new BCryptPasswordEncoder());
	}
	
	
	/*
	@Bean 
	InMemoryUserDetailsManager userDetailsManager() {
		log.info("Step:01");
		UserDetails user = User.builder()
					.username("username")
					.password(new BCryptPasswordEncoder().encode("password"))
					.roles("USER")
					.build();
		
		UserDetails admin = User.builder()
				.username("mia")
				.password(new BCryptPasswordEncoder().encode("ciao"))
				.roles("ADMIN","USER")
				.build();
		
	   return new InMemoryUserDetailsManager(user,admin);	
	} 
	*/
	
    @Bean
    BCryptPasswordEncoder passwordEncoder() {
    	return new BCryptPasswordEncoder(); 
    } 

    @Bean
    @SneakyThrows
    SecurityFilterChain securityFilterChain(HttpSecurity http) {
    	
    	
    	http.authorizeHttpRequests(authz ->
    	{
    		authz.requestMatchers(
    				new AntPathRequestMatcher("/src/main/resources/static/**"),
    				//new AntPathRequestMatcher("/src/main/resources/templates/**"),
    				new AntPathRequestMatcher("/img/**"),
    				new AntPathRequestMatcher("/css/**"),
    				new AntPathRequestMatcher("/forbidden"),
    				new AntPathRequestMatcher("/index"),
    				new AntPathRequestMatcher("/errore"),
    				new AntPathRequestMatcher("/under"),
    				new AntPathRequestMatcher("/login"),
    				new AntPathRequestMatcher("/adminPage"),
    				new AntPathRequestMatcher("/newUtenza"),
    				new AntPathRequestMatcher("/recuperoPsw"),
    				new AntPathRequestMatcher("/nuovoUtente"),
    				new AntPathRequestMatcher("/insertNewUtente"),
    				new AntPathRequestMatcher("/invioRecuperoPsw")
   				).permitAll();
    		
    		
    		authz.requestMatchers(
    				new AntPathRequestMatcher("/riservato")
    		).hasRole("ADMIN");
    		
    		authz.anyRequest().authenticated();
    	
    	
    	}).formLogin(form -> form
    			.loginPage("/login")
    			.loginProcessingUrl("/login")
    			.usernameParameter("username")
    			.successHandler(authenticationSuccessHandler())
    			.failureHandler(authenticationFailureHandler())
    			.permitAll())
    	.exceptionHandling(ex -> ex
    			.accessDeniedHandler(accessDeniedHandler()));
    	
    	return http.build();
    }  

    @Bean
    AuthenticationSuccessHandler authenticationSuccessHandler() {
    	log.info("==CustomSuccessHandler==");
    	return new CustomSuccessHandler();
    }
    
    @Bean
    AuthenticationFailureHandler authenticationFailureHandler() {
    	log.info("==authenticationFailureHandler==");
    	return new CustomAuthenticationFailureHandler();
    }
    
    @Bean
    AccessDeniedHandler accessDeniedHandler() {
     	log.info("=******=accessDeniedHandler=******=");
    	return new CustomAccessDeniedHandler();
    }
    

}
