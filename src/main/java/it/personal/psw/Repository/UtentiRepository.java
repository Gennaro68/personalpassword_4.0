package it.personal.psw.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import it.personal.psw.Entity.ArchivioUtenze;
import it.personal.psw.Entity.Ruoli;
import it.personal.psw.Entity.Utenti;

public interface UtentiRepository extends JpaRepository<Utenti, Long> {
	
	Utenti findByUserid(String userId);

	void save(Ruoli ruoli);
	
	@Query(value="SELECT id FROM utenti WHERE userid=:userid",nativeQuery = true)
	Long findByUserid1(@Param("userid") String userid);
	
}
