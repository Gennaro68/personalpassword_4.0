package it.personal.psw.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import it.personal.psw.Entity.Ruoli;
import it.personal.psw.Entity.Utenti;
import java.util.List;
import java.util.Optional;


public interface RuoliRepository extends JpaRepository<Ruoli, Long> {

	void save(Utenti utente);
	
	public Optional<Ruoli> findById(Long id);
	
	
	
	@Query(value="DELETE FROM ruoli WHERE idutente=:idutente", nativeQuery=true)
	void deleteByIdUtente(@Param("idutente") Long idutente);
	
		
	
}
