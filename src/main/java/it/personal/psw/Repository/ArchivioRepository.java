package it.personal.psw.Repository;

import java.util.List;

import org.springframework.data.jdbc.repository.query.Modifying;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import it.personal.psw.Entity.ArchivioUtenze;
import jakarta.transaction.Transactional;

public interface ArchivioRepository extends JpaRepository<ArchivioUtenze,Long> {
	
	public List<ArchivioUtenze> findByUsernamePp(String username_pp);
	
	//public List<ArchivioUtenze> orderByUsArchivioUtenzes();
	
	@Query(value = "SELECT * FROM `ppdbase`.`archivio_utenze` WHERE utenza = :username", nativeQuery = true)
	List<ArchivioUtenze> findByUsername(@Param("username") String username);
	
  
	@Modifying
	@Transactional
	@Query(value="DELETE FROM archivio_utenze", nativeQuery = true)
	public void deleteAllArchivioUtenze();
	
	
	
	
}
