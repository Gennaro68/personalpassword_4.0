package it.personal.psw.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import it.ObjUtil.VistaAbbonati;
import it.personal.psw.Entity.Abbonato;
import it.personal.psw.Entity.ArchivioUtenze;

public interface AbbonatoRepository extends JpaRepository<Abbonato, Long> {	
	
	@Query(value="SELECT r.idutente,a.cognome,a.nome,a.email,r.ruolo FROM abbonato a,ruoli r WHERE r.idutente=a.utenti_id ",nativeQuery = true)
	List<Object[]> getAbbonati1();
	
	@Query(value="SELECT email FROM abbonato WHERE email =:email", nativeQuery = true)
	String getByEmail(String email);
	
	@Query(value="SELECT utenti_id FROM abbonato WHERE email=:email", nativeQuery=true)
	Long getByUtenti_id(String email);

	@Query(value="DELETE FROM Abbonato where utenti_id=:utenti_id", nativeQuery=true)
	void deledeleteByUtenti_id(Long utenti_id);
	
}
