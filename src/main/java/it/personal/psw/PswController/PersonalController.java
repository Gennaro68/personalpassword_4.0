package it.personal.psw.PswController;

import java.security.SecureRandom;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.ResponseCookie;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import it.ObjUtil.VistaAbbonati;
import it.personal.psw.Entity.Abbonato;
import it.personal.psw.Entity.ArchivioUtenze;
import it.personal.psw.Entity.Ruoli;
import it.personal.psw.Entity.Utenti;
import it.personal.psw.RepositoryImp.AbbonatiImpl;
import it.personal.psw.RepositoryImp.ArchivioImpl;
import it.personal.psw.RepositoryImp.EmailService;
import it.personal.psw.RepositoryImp.RuoliImpl;
import it.personal.psw.RepositoryImp.UtentiImpl;
import jakarta.mail.MessagingException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import lombok.extern.java.Log;

@Controller
@Log
@PropertySource("classpath:application.properties")

public class PersonalController {
	@Value("${form.login.logout.message}")
	private String logout_message;

	private String userValue;

	@Autowired
	ArchivioImpl archivioImpl;

	@Autowired
	AbbonatiImpl abbonatiImpl;

	@Autowired
	RuoliImpl ruoliImpl;

	@Autowired
	// UtentiRepository utenti;
	UtentiImpl utentiImpl;

	@Autowired
	EmailService emailService;

	// per recuperare username
	private void setUserValue(String userValue) {
		this.userValue = userValue;
	}

	// per recuperare username
	private String getUserValue() {
		return this.userValue;
	}

	@GetMapping("/")
	public String slash() {
		return "index";
	}

	@GetMapping("/index")
	public String index() {
		return "index";
	}

	@GetMapping("/recuperoPsw")
	public String recuperoPsw() {
		log.info("==recuperoPsw==");
		return "recuperoPsw";
	}
	
	@GetMapping("/errore")
	public String errore() {
		return "errore";
	}

	@GetMapping("/under")
	public String unDer() {
		return "under";
	}

	@PostMapping("/invioRecuperoPsw")
	/*
	public String invioRecuperoPsw(@RequestParam(name = "nome") String nome,
			@RequestParam(name = "cognome") String cognome, @RequestParam(name = "email") String email,
			@RequestParam(name = "username") String username, @RequestParam(name = "password") String password,
			Model model) {
	*/
	public String invioRecuperoPsw(
			@RequestParam(name = "email") String email,
			@RequestParam(name = "username") String username, 
			@RequestParam(name = "password") String password,
			Model model){
		
		boolean isMail = true;

		log.info("=**=invioRecuperoPsw=**=");
		// Viene fatta questa stramba operazione per vedere se la mail è presente
		
		Long id_utenti = abbonatiImpl.getUtentiIdFromUtentiId(email);
		
		if ((abbonatiImpl.getEmail(email) == null) || (id_utenti == null)) {
			isMail = false;
		}

		log.info("Id--Utenti-->" + id_utenti.toString());
		log.info("isMail---->" + isMail);

		// *******************************************************
		// ******Verifica se la mail è presente*****************
		// *******************************************************

		if (isMail) {
			// Prendi la password desiderata dell utente
			String psw_chiaro = password;

			// Ricostruisci Utenti
			Utenti ut = new Utenti();
			ut.setId(id_utenti);
			ut.setAttivo("Si");
			ut.setPassword(new BCryptPasswordEncoder().encode(password));
			ut.setUserid(username);

			// Fai update di utenti
			utentiImpl.updateUtenti(ut);

			// Invia la mail
			// Solo per test la mail è sempre la stessa per prendere visione della mail che
			// viene inviata.
			email = "graziano.aloisi@gmail.com";
			this.sendMail(email, "= Passpack = Nuova Utenza Generata",
					"La una nuova utenza è:" + username + "\n tua nuova password è: " + psw_chiaro);

			log.info("=**Mail inviata presso utente**=");
			model.addAttribute("message",
					"Controlla la tua casella di posta elettronica la tua utenza è stata aggiornata con successo");

		} else {
			// Spiacente ma la sua utenza non corrisponde
			log.info("=**Utenza verificata e non riconosciuta**=");
			this.sendMail(email, "= Passpack = Nuova utenza non generata",
					"Spiacenti ma non possiamo generare una nuova utenza");
			log.info("=**Mail di rifiuto di registrazione Inviata**=");
			model.addAttribute("message", "Purtroppo la sua utenza non esiste");
		}

		// Fai la pagina con la differente messaggistica in funzione del risultato.

		return "login";
	}

	private void sendMail(String mail, String oggetto, String corpo) {
		try {
			emailService.sendEmail(mail, oggetto, corpo);
			log.info("Email inviata con successo");
		} catch (MessagingException e) {
			log.info("Errore invio email " + e.getMessage());
		}
	}

	@GetMapping("/nuovoUtente")
	public String nuovoUtente() {

		return "nuovoUtente";
	}

	@PostMapping("/insertNewUtente")
	public String insertNewUtente(@RequestParam(name = "nome", required = true) String nome,
			@RequestParam(name = "cognome", required = true) String cognome,
			@RequestParam(name = "mail", required = true) String mail,
			@RequestParam(name = "user", required = true) String user,
			@RequestParam(name = "password", required = true) String password, Model model) {

		Long id_utenti;
		boolean ifRegistrato=false;
		 
		//Controlliamo se l' utente si è già registrato con la stessa mail
		String mailAbbonato = abbonatiImpl.getEmail(mail);   
		
		if(mailAbbonato!=null) {
			ifRegistrato=true;	
			model.addAttribute("error", "Attenzione! Mail già censita");
			log.info("Attenzione! Mail già censita");
		}
		if(password.length()<=8) {
			ifRegistrato=true;
			model.addAttribute("error", "Attenzione! Password corta");
			log.info("Inserimento non concesso password è corta ");
		}
		
		
		
		if(ifRegistrato==false) {
			
			log.info("==/insertNewUtente==");
			log.info(nome + "	" + cognome + "	" + mail + "	" + user + "	" + password);
			log.info("==>Salvataggio concesso!!<==");
			
			// ==crei entità Utenti
			Utenti singoloUtente = new Utenti();
	
			// ==Popoli
			singoloUtente.setAttivo("Si");
			singoloUtente.setPassword(new BCryptPasswordEncoder().encode(password));
			singoloUtente.setUserid(user);
			
			// ==Salvi Utenti
			id_utenti = utentiImpl.inserisciUtenti(singoloUtente);
	
			// ==Crei entità Abbonato
			Abbonato abbonato = new Abbonato();
	
			// ==La popoli
			abbonato.setCognome(cognome);
			abbonato.setEmail(mail);
			abbonato.setNome(nome);
	
			// ==Con il SetUtente metti in relazione
			// ==le due tabelle Utenti-Abbonato con relazione 1:1
			abbonato.setUtente(singoloUtente);
			// ==Salviamo la riga dell abbonato
			abbonatiImpl.save(abbonato);
	
			// ==Crei una entità Ruoli
			Ruoli ruoli = new Ruoli();
			ruoli.setRuolo("USER");
	
			// ==Metti in relazione il ruolo con gli Utenti
			ruoli.setUtente(singoloUtente);
	
			ruoliImpl.save(ruoli);
	
			// *****************************************************
			// ***Quando viene abilitata anche la ADMIN**************
			// *****************************************************
			
			/*
			 Ruoli entityRuoli_02 = new Ruoli(); //Doppio
			 entityRuoli_02.setRuolo("ADMIN"); entityRuoli_02.setUtente(singoloUtente);
			 
			 ruoliImpl.save(entityRuoli_02);
			*/ 
		}
		
		
		return "login";
	}

	// ---Operazioni di servizio: CANCELLA - MODIFICA
    // ---Operazioni di cancellazione di una singola password co utenza registrata
	@PostMapping("/cancella/{id}")
	public String cancellaRigaArchivioUtenze(@PathVariable Integer id, Model model) {

		log.info("cancellaRigaArchivioUtenze->" + id.toString());
		
		archivioImpl.deleteRigaArchivioUtenze(id);

		// legge le utenze
		List<ArchivioUtenze> my_archivio = null;
		my_archivio = archivioImpl.findByUsername(this.getUserValue());
		
		// Passa al model
		model.addAttribute("pippo", my_archivio);

		return "adminPage";
	}
	
	
	@PostMapping("/riservato")
	public String cancellaRiservato(@RequestParam("idUtente")Long idUtente, Model model) {
		
		log.info("valore di idUtente ---> "+idUtente.toString());
		log.info("cancellaRiservato-->"+idUtente);
		abbonatiImpl.deleteAbbonato(idUtente);
		ruoliImpl.delete(idUtente);
		utentiImpl.deleteByuserid(idUtente);
		
		List<VistaAbbonati> myvistaAbbonati = abbonatiImpl.getAbbonati1();
		model.addAttribute("vistaRiserva", myvistaAbbonati);
		
		return "riservato";
	}
	
	
	@GetMapping("/riservato")
	public String getAbbonati(Model model) {
		log.info("==abbonatiImpl.getAbbonati()==");		
		//id_utente,cognome,nome,email,ruolo
		List<VistaAbbonati> myvistaAbbonati = abbonatiImpl.getAbbonati1();
		// Passa al model
		model.addAttribute("vistaRiserva", myvistaAbbonati);
		return "riservato";
	}	
	

	@GetMapping("/login")
	public String login(@RequestParam(name = "messaggio", required = false) String messaggio, Model model) {
		// La stringa messaggio viene popolata quando ci sono dei problemi con
		// l' identificazione dell utente e l' invocazione parte
		// da:CustomAuthenticationFailureHandler

		// Quando il messaggio è popolato lo passa al model lo assegna ad error
		if (messaggio != null)
			model.addAttribute("error", messaggio.toString());

		return "login";
	}

	@PostMapping("/newUtenza")
	public String newUtenza(@RequestParam(name = "usernameForm", required = true) String usernameForm,
			@RequestParam(name = "passwordForm", required = true) String passwordForm,
			@RequestParam(name = "descrizioneForm") String descrizioneForm, Model model) {
		log.info("==/newUtenza==");

		if (usernameForm.equals(null))
			log.info("campo Username obbligatorio");
		if (passwordForm.equals(null))
			log.info("campo Password obbligatorio");
		if (descrizioneForm.length() >= 255)
			log.info("Attenzione il campo  descrizione non supporta più di 255 caratteri!!!");

		log.info(" usernameForm-->" + usernameForm + " passwordForm-->" + passwordForm + " descrizioneForm-->"
				+ descrizioneForm + " valore User-->" + this.getUserValue());

		ArchivioUtenze archUtenze = new ArchivioUtenze();
		// popola l' oggetto
		archUtenze.setUsernamePp(usernameForm);
		archUtenze.setPasswordPp(passwordForm);
		archUtenze.setDescrizionePp(descrizioneForm);
		archUtenze.setUtenza(this.getUserValue());

		log.info(" usernameForm-->" + usernameForm + " passwordForm-->" + passwordForm + " descrizioneForm-->"
				+ descrizioneForm + " valore User-->" + this.getUserValue());

		archivioImpl.addArchivioUtenze(archUtenze);
		// legge le utenze
		List<ArchivioUtenze> my_archivio = null;
		my_archivio = archivioImpl.findByUsername(this.getUserValue());

		// Passa al model
		model.addAttribute("pippo", my_archivio);

		return "adminPage";
	}

	// updateArchivio
	@PostMapping("/updateUtenza")
	public String updateUtenza(@RequestParam(name = "usernameForm", required = true) String usernameForm,
			@RequestParam(name = "passwordForm", required = true) String passwordForm,
			@RequestParam(name = "descrizioneForm") String descrizioneForm, @RequestParam(name = "id") String id,
			Model model) {
		log.info("==/updateUtenza==");

		ArchivioUtenze archUtenze = new ArchivioUtenze();
		// popola l' oggetto
		archUtenze.setUsernamePp(usernameForm);
		archUtenze.setPasswordPp(passwordForm);
		archUtenze.setDescrizionePp(descrizioneForm);
		archUtenze.setUtenza(this.getUserValue());
		archUtenze.setId(Integer.parseInt(id));

		log.info(" usernameForm-->" + usernameForm + " passwordForm-->" + passwordForm + " descrizioneForm-->"
				+ descrizioneForm + " valore id-->" + id);

		archivioImpl.addArchivioUtenze(archUtenze);

		// legge le utenze
		List<ArchivioUtenze> my_archivio = null;
		my_archivio = archivioImpl.findByUsername(this.getUserValue());

		// Passa al model
		model.addAttribute("pippo", my_archivio);

		return "adminPage";
	}

	@GetMapping("/adminPage")
	public String adminPage(Model model, @CookieValue(name = "user-id") String name) {
		log.info("==/adminPage==");
		log.info("Nome del cookie======>" + name);

		if (name != null) {
			model.addAttribute("utenza", name);
			this.setUserValue(name);
		}

		// legge le utenze
		List<ArchivioUtenze> my_archivio = null;
		my_archivio = archivioImpl.findByUsername(this.getUserValue());

		// Passa al model
		model.addAttribute("pippo", my_archivio);

		return "adminPage";
	}

//	@GetMapping("/riservato")
//	public String getAbbonati(Model model) {
//		log.info("==abbonatiImpl.getAbbonati()==");		
//		//id_utente,cognome,nome,email,ruolo
//		
//		List<VistaAbbonati> myvistaAbbonati = abbonatiImpl.getAbbonati1();
//		// Passa al model
//		model.addAttribute("vistaRiserva", myvistaAbbonati);
//
//		return "riservato";
//	}	

	@GetMapping("/logout")
	public String getLogout(HttpServletRequest request, HttpServletResponse response) {
		// Prende la sessione e la uccide
		HttpSession sessione = request.getSession(false);

		// Accede e modifica alle informazioni riservate
		SecurityContextHolder.clearContext();

		// prende la sessione e la invalida
		if (sessione != null)
			sessione.invalidate();

		// Cicla tutti i cookie ed azzera la vita
		for (jakarta.servlet.http.Cookie cookie : request.getCookies()) {
			cookie.setMaxAge(0);
		}

		// prende il cookie specifico e lo azzera
		ResponseCookie springCookie = ResponseCookie.from("user-id", "").maxAge(0).build();

		response.addHeader(org.springframework.http.HttpHeaders.SET_COOKIE, springCookie.toString());
		log.info(
				"Sono dentro al Personal controller ed eseguo la redirect a:redirect:/login?messaggio=logout eseguito correttamente");

		// logout_message = logout eseguito correttamente
		return "redirect:/login?messaggio=" + logout_message;
	}

	public String testaValore(String xx) {
		return xx;
	}

}
