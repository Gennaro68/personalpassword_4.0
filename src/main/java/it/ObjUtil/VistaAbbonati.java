package it.ObjUtil;

import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class VistaAbbonati {
	private String cognome; 
	private String nome;
	private String email;
	private String ruolo;
	private String userid;
	private String attivo;
	private String id;
	private String id_utente;
	
	@Override
	public String toString() {
		return "VistaAbbonati [cognome=" + cognome + ", nome=" + nome + ", email=" + email + ", ruolo=" + ruolo
				+ ", userid=" + userid + ", attivo=" + attivo + ", id=" + id + ", id_utente=" + id_utente + "]";
	}
	
	
	
}
