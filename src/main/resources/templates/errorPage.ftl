<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pagina di errore</title>
    <style>
        body {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            height: 100vh;
            margin: 0;
        }
        
        #logo {
            width: 200px; /* Imposta la larghezza desiderata per il logo */
            height: auto; /* L'altezza si adatta automaticamente alla larghezza */
        }

        #construction-text {
            font-size: 24px;
            margin-top: 20px;
            font-family: Verdana, sans-serif; 
    		font-weight: bold; 
        }
    </style>    
</head>
<body>
    
    <h1>Pagina di errore personalizzata:</h1>
    <h2>Sei un Pirla credo che possa bastare</h2>
    
    <a href="/login"> <p id="construction-text">Torna al Menù principale</p></a>
    
</body>
</html>
