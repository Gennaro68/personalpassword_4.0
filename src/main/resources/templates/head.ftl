 
  <link href="css/style.css" rel="stylesheet">
  <nav class="navbar navbar-expand-lg bg-body-tertiary">
  
  <div class="container-fluid">
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
        <a class="navbar-brand" href="#" ><h2><b>Personal Pass</b></h2></a>
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li>
            <a class="nav-link active" aria-current="page" href="/index">
                <button type="button" class="btn btn-outline-primary" ><b>Index</b></button>
            </a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="/login">
              <button type="button" class="btn btn-outline-primary" ><b>Login</b></button>
          </a>
        </li>
        
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="/adminPage">
              <button type="button" class="btn btn-outline-primary" ><b>Elenco Utenze</b></button>
          </a>
        </li>
        
        
        
        
        <li>
            <a class="nav-link active" aria-current="page" href="/riservato">
                <button type="button" class="btn btn-outline-primary" ><b>Superuser</b></button>
            </a>
        </li>  
        <li>
            <a class="nav-link active" aria-current="page" href="/logout">
                <button type="button" class="btn btn-outline-primary" ><b>Logout</b></button>
            </a>
        </li>        
    </ul>
    </div>
  </div>
</nav>
