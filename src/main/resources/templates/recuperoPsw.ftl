<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>recuperoPsw.ftl</title>
     <#include "bootstrap_inc.ftl">
</head>
<body>
    <#include "head.ftl">
    <div align="center">
    	 <h1>Form di recupero Password</h1>
         <h3 >Compila i campi sottostanti e riceverai a breve la nuova password</h3>
    </div>		
    	</br></br>
		<form action="/invioRecuperoPsw" method="POST">
		    <!--
		    <div class="row justify-content-center">
		    	<div class="col-md-4">
			        <label for="labelNome">Nome</label>
			        <input type="text" class="form-control" id="nome" name="nome" aria-describedby="inputNome" placeholder="Nome">
		        </div>
		    </div>
		    <div class="row justify-content-center">
		    	<div class="col-md-4">
			        <label for="labelCognome" style="padding-top:10px;">Cognome</label>
			        <input type="text" class="form-control"  name="cognome" id="cognome" placeholder="Cognome">
		        </div>
		    </div>
		    -->
		    <div class="row justify-content-center">
		    	<div class="col-md-4">
			        <label for="labelEmail" style="padding-top:10px;">Email</label>
			        <input type="text" class="form-control" name="email" id="email" placeholder="Email">
		        </div>
		    </div>
		    <div class="row justify-content-center">
		    	<div class="col-md-4">
			        <label for="labelUsername" style="padding-top:10px;">Username</label>
			        <input type="text" class="form-control" name="username" id="username" placeholder="Username">
		        </div>
		    </div>
		    <div class="row justify-content-center">
		    	<div class="col-md-4">
			        <label for="labelUsername" style="padding-top:10px;">Password</label>
			        <input type="password" class="form-control" name="password" id="password" placeholder="Password">
		        </div>
		    </div>		    
		    <div class="row justify-content-center">
		    	<div class="col-md-4" style="padding-top:30px;">
			        <button type="submit" class="btn btn-primary" >Submit</button>
		        </div>
		    </div>
			<input type="HIDDEN" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		</form>
		<br>    
</body>
</html>
