package it.personal.psw;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.ClassOrderer.OrderAnnotation;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.context.WebApplicationContext;

import it.personal.psw.PswController.PersonalController;

@SpringBootTest()
@ContextConfiguration(classes=PersonalPasswordApplication.class)

public class TestPersonalController {
	
	
	private MockMvc mvc;
	
	@Autowired
	private WebApplicationContext wac;
	
	@BeforeEach
	public void setup() {
		mvc=MockMvcBuilders.webAppContextSetup(wac).build();
	}
	
	@Test
	public void testLogin() throws Exception {
		mvc.perform(get("/login")
				.contentType(MediaType.TEXT_HTML))
		.andExpect(status().isOk()).andDo(print());
	}
	
	@Test
	public void testNewUtenza() throws Exception {
		//La pagina viene reindirizzata
		mvc.perform(post("http://localhost:8080/newUtenza")
				.contentType(MediaType.TEXT_HTML))
		.andExpect(status().is(400)).andDo(print());
	}
	
	@Test
	public void testAdminPage() throws Exception {
		mvc.perform(get("/adminPage")
				.contentType(MediaType.TEXT_HTML))
		.andExpect(status().is(400)).andDo(print());
	}
	
	@Test
	public void index() throws Exception {
		mvc.perform(get("/")
				.contentType(MediaType.TEXT_HTML))
		.andExpect(status().isOk()).andDo(print());
	}
	
	
	
	
	
}
