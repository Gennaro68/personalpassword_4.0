package it.personal.psw;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.fail;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.io.IOException;

import org.hamcrest.Matcher;
import org.json.JSONException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.ClassOrderer.OrderAnnotation;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.aop.ThrowsAdvice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.context.WebApplicationContext;

import it.personal.psw.PswController.PersonalController;
import lombok.extern.java.Log;


@ContextConfiguration(classes=PersonalPasswordApplication.class)
@SpringBootTest()
@Log
//@TestMethodOrder(CustomOrderer.class)
public class TestMetodiCrud {
	
	private MockMvc mockMvc;
	
	@Autowired
	WebApplicationContext wac;
	
	@BeforeEach
	public void setup() throws JSONException,IOException {
		//mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
		mockMvc = MockMvcBuilders.standaloneSetup(new PersonalController()).build();
		
	}
		
	//=====Gestione Password inserite=====
	
	
	
	@Test
	public void Test02_testNewUtenza() throws Exception {
		String valore="Pippo fa la pizza";
		String dest="ciao";
		
		mockMvc.perform(MockMvcRequestBuilders.get("/recuperoPsw?nominativo=\"Pippo fa la pizza\"&dest=\"ciao\"")
		 		.accept(MediaType.TEXT_HTML))
 				.andExpect(status().isOk())
 				.andExpect(MockMvcResultMatchers.content().string('"'+valore+'"'));
		
	}
	
//	@Test
//	public void Test03_testAzzeraDb() {
//		log.info("TestA_testAzzeraDb");
//		fail("Metodo non implementato");
//	}
//	
	
	//	@Test
//	public void Test03_testUpdateUtenza() {
//		fail("Metodo non implementato");
//	}
//	
//	@Test
//	public void Test04_testCancellaUtenza() {
//		fail("Metodo non implementato");
//	}
//	
//	//===SuperUser================
//	
//	
//	//===Archivio==Utilizzatori===
//	//===Sviluppare prima la entity che definisce lo 
//	//===il nuovo Utilizzatore del servizio==
//	//===ricordiamoci poi che và a popolare anche la 
//	//===delicata tabella utenti e ruoli
//	
//	@Test
//	public void Test05_testVerificaEsistenzaUtilizzatore() {
//		fail("Metodo non implementato");
//	}
//	
//	@Test
//	public void Test06_testInserisciNuovoUtilizzatoreServizioPagSuperUser() {
//		fail("Metodo non implementato");
//	}
//	
//	@Test
//	public void Test07_testModificaNuoviUtilizzatoreServizio() {
//		fail("Metodo non implementato");
//	} 	
//    
//	@Test
//	public void Test08_testCancellaNuoviUtilizzatoreServizio() {
//		fail("Metodo non implementato");
//	}
//	
//	@Test
//	public void Test09_testSospendiNuoviUtilizzatoreServizio() {
//		fail("Metodo non implementato");
//	}
//	
//	
//	
//	//====Creazione password=Nuovo_Utente====
//	@Test
//	public void Test10_testGeneraNuovoUtilizzatorePsw() {
//		fail("Metodo non implementato");
//	}
//    
//	@Test
//	public void Test11_testCambioPswUtilizzatoreVecchio() {
//		fail("Metodo non implementato");
//	}
//	
//	@Test
//	public void Test12_testInvioMailNuovaPswCreata() {
//		fail("Metodo non implementato");
//	}
	
	
	

} 

