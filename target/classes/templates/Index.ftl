<!DOCTYPE html>
<html lang="it">
<head>
    <title>Index.ftl</title>
    <#include "bootstrap_inc.ftl">      
</head>
<body>
	<#include "head.ftl">	
	<div class="px-4 py-5 my-3 text-center">
	    <h1 class="display-5 fw-bold text-body-emphasis"><b>Personal Pass</b></h1>
	    <div class="col-lg-6 mx-auto">
		    <div class="alert alert-warning" role="alert">
		      <p class="lead mb-2"><b>La tua password Ovunque!!!</b></p>
		    </div>    
	        <div align="center"><img src="img/logo.png" alt="logo" width="200" height="200"></div>      
	      </br>
	      <p class="lead mb-4">
		      Nel mondo frenetico di oggi, il tuo 'Personal Pass Ovunque' è il tuo biglietto per 
		      l'accesso senza soluzione di continuità a tutto ciò che ami. Non importa dove ti trovi, 
		      il potere del tuo 'Personal Pass' ti accompagna ovunque. 
		      È la chiave per aprire porte inesplorate e creare esperienze personali indimenticabili. 
		      Con il tuo 'Personal Pass', il mondo è il tuo playground e le opportunità sono infinite.
		      
	      </p>      
        </div>
   </div>
   <div align="left">
   		<b>per entrare con utenza SuperUser</b> 
		      <UL>
			      <LI>pippofalapizza</LI>
			      <LI>pippofalapizza</LI>
		      </UL>
		<b>per entrare con utenza admin:</b>
		      <UL>
			      <LI>pippo</LI>
			      <LI>pippo</LI> 
		      </UL>
             
   </div>	               
    
</body>
</html>
