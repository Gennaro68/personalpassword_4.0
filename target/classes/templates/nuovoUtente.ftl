<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>nuovoUtente.ftl</title>
     <#include "bootstrap_inc.ftl">
     
     <script>
			$(document).ready(function() {
			  $("#password").on("input", function() {
			    
			    var inputText = $(this).val();
			    if (inputText.length < 5) {
			      $("#alertMessage").show();
			    } else {
			      $("#alertMessage").hide();
			    }
			  });
			});
	 </script>
</head>
<body>
    <#include "head.ftl">
    <div id="alertMessage" class="alert alert-info" style="display: none;">
		  La lunghezza deve essere almeno 5 caratteri.
	</div>
    
    <div align="center"><h1>Form di registrazione nuovo utente</h1></div>
	    <div align="center">
		    <h3>
		    	A breve riceverai una mail di conferma la quale ti comunicherà il permesso per utilizzare i nostri servizi
		    </h3>
	    </div>    
    </br>   
		<form action="/insertNewUtente" method="POST">
			<div class="row justify-content-center">
		    	<div class="col-md-4">
			        <label for="labelNome" style="padding-top:10px;">Nome</label>
			        <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome" required>
		        </div>
		    </div>
			<div class="row justify-content-center">
		    	<div class="col-md-4">
			        <label for="labelCognome" style="padding-top:10px;">Cognome</label>
			        <input type="text" class="form-control" id="cognome" name="cognome"  placeholder="Cognome" required>
		        </div>
		    </div>
		    <div class="row justify-content-center">
		    	<div class="col-md-4">
			        <label for="labelMailRiferimento" style="padding-top:10px;">Mail di riferimento</label>
			        <input type="mail" class="form-control" id="mail" name="mail" aria-describedby="inputMail" pattern="[^@]+@[^@]+\.[a-zA-Z]{2,}" placeholder="Mail" required>
		        </div>
		    </div>
			<div class="row justify-content-center">
		    	<div class="col-md-4">
			        <label for="labelUser" style="padding-top:10px;">User</label>
			        <input type="text" class="form-control" id="user" name="user" placeholder="User" required>
		        </div>
		    </div>
			<div class="row justify-content-center">
		    	<div class="col-md-4">
			        <label for="labelPassword" style="padding-top:10px;">Password</label>
			        <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
		        </div>
		    </div>
		    <div class="row justify-content-center">
		    	<div class="col-md-4" style="padding-top:30px;">
			        <button type="submit" class="btn btn-primary" >Submit</button>
		        </div>
		    </div>
		    <input type="HIDDEN" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		</form>
		<br>    
		<br>    
</body>
</html>

</html>
