<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>riservato.ftl</title>
    
     <#include "bootstrap_inc.ftl">
     
    <script>
	    function apriModificaForm(button) {
	        // Ottieni i valori dei campi dai dati attribuiti al pulsante
	        const username = button.getAttribute('data-username');
	        const password = button.getAttribute('data-password');
	        const description = button.getAttribute('data-description');
	        const id = button.getAttribute('data-id');                    
	
	        // Popola i campi del modulo Bootstrap
	        document.getElementById('modalUsername').value = username;
	        document.getElementById('modalPassword').value = password;
	        document.getElementById('modalDescription').value = description;
	        document.getElementById('modalId').value = id; 
	
	        // Mostra il modulo Bootstrap
	        $('#modificaModal').modal('show');
	    }
	
	</script>
	
     <style>
    	  .text-center {
			  text-align: center;
			  position: relative;
			  top: 40px;
		  }
          .new-utenza{
              
              margin-left: 40px;
              top: 90px; 
          }
    
    </style>
                
</head>
<body>


	<#include "head.ftl">
    
    <!-- Button trigger modal -->
	
	    <table class="table table-bordered text-center table-condensed table-striped ">
	    <thead>
	      <tr>
	        <th scope="col">#</th>
	        <th scope="col">id_Utente</th>
	        <th scope="col">Cognome</th>
	        <th scope="col">Nome</th>
	        <th scope="col">Email</th>
	        <th scope="col">Ruolo</th>
	      </tr>
	    </thead>
	    <tbody>           
		     <!--idutente,cognome,nome,email,ruolo-->		     
		     <#assign numero = 1>
		     <#if vistaRiserva??>
			     <#list vistaRiserva as arch>	  
			      <tr>
			        <th scope="row">${numero}</th>
			        <td>${arch.id_utente}</td>
			        <td>${arch.cognome}</td>
			        <td>${arch.nome}</td>
			        <td>${arch.email}</td>
					<td>${arch.ruolo}</td>
			        <td>
			            <form action="/riservato" method="post">
			            	<input type="HIDDEN" name="idUtente" value="${arch.id_utente}">
			            
					        <button type="submit" class="btn btn-danger btn-sm delete-button" data-id="">
					        		<i class="bi bi-trash"></i>&nbsp;Cancella
					        </button>
					    <input type="HIDDEN" name="${_csrf.parameterName}" value="${_csrf.token}"/>    
				        </form>
			        </td>         
			      </tr>
			     <#assign numero = numero + 1>
			     </#list>
		     <#else>
		        <!--Quando è tutto vuoto!!-->
		     	<tr>
		        <th scope="row">&nbsp;</th>
		        <td>&nbsp;</td>
		        <td>&nbsp;</td>
		        <td>&nbsp;</td>
		        <td>&nbsp;</td>
		        <td>&nbsp;</td>
		        <td><button type="button" class="btn btn-danger btn-sm" value="00"><i class="bi bi-trash"></i>&nbsp;Cancella</button></td>
		      </tr>	  
		     </#if>	
	    </tbody>
	    </table>
   <br>
   <br>
   <br> 
	<nav aria-label="Page navigation example" class="new-utenza">
	  <ul class="pagination">
	    <li class="page-item">
	      <a class="page-link" href="#" aria-label="Previous">
	        <span aria-hidden="true">&laquo;</span>
	      </a>
	    </li>
	    <li class="page-item"><a class="page-link" href="#">1</a></li>
	    <li class="page-item"><a class="page-link" href="#">2</a></li>
	    <li class="page-item"><a class="page-link" href="#">3</a></li>
	    <li class="page-item">
	      <a class="page-link" href="#" aria-label="Next">
	        <span aria-hidden="true">&raquo;</span>
	      </a>
	    </li>
	  </ul>
	</nav>
</body>
</html>