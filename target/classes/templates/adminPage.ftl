<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>adminPage.ftl</title>
    
     <#include "bootstrap_inc.ftl">
     
    <script>
	    function apriModificaForm(button) {
	        // Ottieni i valori dei campi dai dati attribuiti al pulsante
	        const username = button.getAttribute('data-username');
	        const password = button.getAttribute('data-password');
	        const description = button.getAttribute('data-description');
	        const id = button.getAttribute('data-id');                    
	
	        // Popola i campi del modulo Bootstrap
	        document.getElementById('modalUsername').value = username;
	        document.getElementById('modalPassword').value = password;
	        document.getElementById('modalDescription').value = description;
	        document.getElementById('modalId').value = id; 
	
	        // Mostra il modulo Bootstrap
	        $('#modificaModal').modal('show');
	    }
	
	</script>
     
     
     
     <style>
    	  .text-center {
			  text-align: center;
			  position: relative;
			  top: 40px;
		  }
          .new-utenza{
              
              margin-left: 40px;
              top: 90px; 
          }
    
    </style>
       
                
</head>
<body>


	<#include "head.ftl">
    <!--
    <button type="button" class="btn btn-primary new-utenza" data-target="#exampleModal"><i class="bi bi-database-up"></i>&nbsp;Inserisci Una Nuova Password</button> 
    -->
    <!-- Button trigger modal -->
	<button type="button" class="btn btn-primary new-utenza" data-toggle="modal" data-target="#exampleModal">
	  <i class="bi bi-database-up"></i>&nbsp;Inserisci Una Nuova Password
	</button>  
	 
	    <table class="table table-bordered text-center table-condensed table-striped ">
	    <thead>
	      <tr>
	        <th scope="col">#</th>
	        <th scope="col">Username</th>
	        <th scope="col">Password</th>
	        <th scope="col">Descrizione</th>
	        <th scope="col">Modifica</th>
	        <th scope="col">Cancella</th>
	      </tr>
	    </thead>
	    <tbody>           
		     
		     <#assign numero = 1>
		     <#if pippo??>
		     <#list pippo as arch>	  
		      <tr>
		        <th scope="row">${numero}</th>
		        <td>${arch.usernamePp}</td>
		        <td>${arch.passwordPp}</td>
		        <td>${arch.descrizionePp}</td>
		        <td>
		        	 
		        	 <button type="button" 
		        	  	data-username=${arch.usernamePp} 
		        	  	data-password=${arch.passwordPp} 
		        	  	data-description=${arch.descrizionePp}
		        	  	data-id=${arch.id}
		        	 class="btn btn-success btn-sm" value="00" onclick="apriModificaForm(this)">
		        	       	<i class="bi bi-arrow-clockwise" ></i>&nbsp;Modifica
		        	 </button>
		        	 
		        </td>    
		        <td>
		            <form action="/cancella/${arch.id}" method="post">
				        <button type="submit" class="btn btn-danger btn-sm delete-button" data-id="${arch.id}">
				        		<i class="bi bi-trash"></i>&nbsp;Cancella
				        </button>
				    <input type="HIDDEN" name="${_csrf.parameterName}" value="${_csrf.token}"/>    
			        </form>
		        </td>         
		      </tr>
		     <#assign numero = numero + 1>
		     </#list>
		     <#else>
		        <!--Quando è tutto vuoto!!-->
		     	<tr>
		        <th scope="row">&nbsp;</th>
		        <td>&nbsp;</td>
		        <td>&nbsp;</td>
		        <td>&nbsp;</td>
		        <td><button type="button" class="btn btn-success btn-sm" value="00"><i class="bi bi-arrow-clockwise"></i>&nbsp;Modifica</button></td>
		        <td><button type="button" class="btn btn-danger btn-sm" value="00"><i class="bi bi-trash"></i>&nbsp;Cancella</button></td>
		      </tr>	  
		     </#if>	  
	    
	    </tbody>
	    </table>
  
   <br>
   <br>
   <br> 
   
	<nav aria-label="Page navigation example" class="new-utenza">
	  <ul class="pagination">
	    <li class="page-item">
	      <a class="page-link" href="#" aria-label="Previous">
	        <span aria-hidden="true">&laquo;</span>
	      </a>
	    </li>
	    <li class="page-item"><a class="page-link" href="#">1</a></li>
	    <li class="page-item"><a class="page-link" href="#">2</a></li>
	    <li class="page-item"><a class="page-link" href="#">3</a></li>
	    <li class="page-item">
	      <a class="page-link" href="#" aria-label="Next">
	        <span aria-hidden="true">&raquo;</span>
	      </a>
	    </li>
	  </ul>
	</nav>
	
	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel"><i class="bi bi-database-up"></i>&nbsp;<b>Inserisci nuova utenza</b></h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <!--Form di immissione password-->
	        	<form action="/newUtenza" method="post">
				  <div class="form-group">
				    <label for="forUsername">Username<span>*</span></label>
				    <input type="username" name="usernameForm" class="form-control" id="usernameForm" aria-describedby="emailHelp" placeholder="Username" required>
				  </div>
				  <div class="form-group">
				    <label for="forPassword">Password<span>*</span></label>
				    <input type="password" name="passwordForm" class="form-control" id="passwordForm" placeholder="Password" required>
				  </div>
				  <div class="form-group">
				    <label for="forTextarea">Descrizione</label>
				    <textarea class="form-control" name="descrizioneForm" id="descrizioneForm" rows="3" maxlength="255"></textarea>
				  </div>
								  
				  <button type="submit" class="btn btn-primary">Submit</button>
				  
				  <input type="HIDDEN" name="${_csrf.parameterName}" value="${_csrf.token}"/>
				</form>
	        <!--fine del form di immissione-->
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	  </div>
	</div>
	<!-- FINE Modal -->
	
	<!--Modal Update form-->
		<div class="modal fade" id="modificaModal" tabindex="-1" role="dialog" aria-labelledby="modificaModalLabel" aria-hidden="true">
		    
		    <div class="modal-dialog" role="document">
		        <div class="modal-content">
		            <div class="modal-header">
		                <h5 class="modal-title" id="modificaModalLabel"><i class="bi bi-database-up"></i>&nbsp;<b>Aggiorna dati Utenza</b></h5>
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                    <span aria-hidden="true">&times;</span>
		                </button>
		            </div>
		            <div class="modal-body">
		                
		                <form action="/updateUtenza" method="post">
		                    <div class="form-group">
		                        <label for="username">Username:</label>
		                        <input type="text" class="form-control" id="modalUsername" name="usernameForm" placeholder="Nuovo Username">
		                    </div>
		                    <div class="form-group">
		                        <label for="password">Password:</label>
		                        <input type="password" class="form-control" id="modalPassword" name="passwordForm" placeholder="Nuova Password">
		                    </div>
		                    <div class="form-group">
		                        <label for="description">Descrizione:</label>
		                        <textarea class="form-control" id="modalDescription" name="descrizioneForm" rows="3" placeholder="Nuova Descrizione"></textarea>
		                    </div>
		                		<input type="HIDDEN" name="id" id="modalId">
		                	
		                	<button type="submit" class="btn btn-primary">Submit</button>
		                	
		                	
				  			<input type="HIDDEN" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		                </form>
		            
		            </div>
		            <div class="modal-footer">
		                <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
		                <!--<button type="button" class="btn btn-success" onclick="aggiornaUtente()">Aggiorna</button>-->
		            </div>
		        </div>
		    </div>
		</div>
    <!--Fine form di update-->
    
</body>
</html>