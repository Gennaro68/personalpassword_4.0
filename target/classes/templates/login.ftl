<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <title>login.ftl</title>
      <#include "bootstrap_inc.ftl">
      <style>
         .container{
         padding-left:0
         }
      </style>
       
       <script>
			$(document).ready(function() {
			  $("#password").on("input", function() {
			    
			    var inputText = $(this).val();
			    if (inputText.length < 5) {
			      $("#alertMessage").show();
			    } else {
			      $("#alertMessage").hide();
			    }
			  });
			});
	   </script>
   
   </head>
   <body>
      <#include "head.ftl">
      
		<div id="alertMessage" class="alert alert-info" style="display: none;">
		  La lunghezza deve essere almeno 5 caratteri.
		</div>
		
      <div class="wrapper login-1">
         <div class="container" style="padding-right:20px">
            <div class="col-left" >
               <div class="login-text">
                  <h2>
                     <img src="img/logo.png" width="40" height="40"/>
                     Personal Pass
                  </h2>
                  </br>	
                  <a class="btn" href="/nuovoUtente">Nuovo Utente</a>
               </div>
            </div>
            <div class="col-right" >
               <div class="login-form" >
                  <h2>Login</h2>
                  <form action="/login" method="post">
                     <p>
                        <label>Username<span>*</span></label>
                        <input name="username" id="username" type="text" placeholder="Username or Email" required >
                     </p>
                     <p>
                        <label>Password<span>*</span></label>
                        <input name="password" id="password" type="password" placeholder="Password" required >
                     </p>
                     <p>
                        <input type="submit" value="Invio" />
                     </p>
                     <p>
                        <a href="/recuperoPsw">Password Dimenticata?</a>
                     </p>
                     <#if _csrf??>
                     	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
                     <#else>
                     	<!-- Valori fittizi per i test -->
                     	<input type="hidden" name="fake_parameter_name" value="fake_token">
                     </#if>
                  </form>
                  <span style="color: red;">
                     <#if error?has_content>
	                     <b>
	                        <nobr>
	                           <p style="color: red;">${error?html}</p>
	                        </nobr>
	                     </b>
	                     </#if>
                  </span>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>
